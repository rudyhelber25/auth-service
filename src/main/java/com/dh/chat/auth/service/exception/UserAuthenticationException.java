package com.dh.chat.auth.service.exception;

/**
 * @author Santiago Mamani
 */
public class UserAuthenticationException extends RuntimeException {

    public UserAuthenticationException() {
    }

    public UserAuthenticationException(String message) {
        super(message);
    }
}
