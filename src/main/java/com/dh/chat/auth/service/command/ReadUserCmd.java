package com.dh.chat.auth.service.command;

import com.dh.chat.auth.service.client.users.model.User;
import com.dh.chat.auth.service.client.users.service.UserModuleService;
import com.dh.chat.auth.service.exception.UserAuthenticationException;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class ReadUserCmd implements BusinessLogicCommand {

    @SuppressWarnings("unused")
    private static Logger LOG = LoggerFactory.getLogger(ReadUserCmd.class);

    @Setter
    private String password;

    @Setter
    private String email;

    @Getter
    private User user;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private UserModuleService service;

    @Override
    public void execute() {
        try {
            user = service.findUserByEmail(email);
        } catch (Exception exception) {
            LOG.info("Unable to locate account for username = " + email);
            throw new UserAuthenticationException("Unable to locate account for username = " + email);
        }

        if (!encoder.matches(password, user.getPassword())) {
            throw new UserAuthenticationException("Invalid credentials [username or password]");
        }
    }
}
