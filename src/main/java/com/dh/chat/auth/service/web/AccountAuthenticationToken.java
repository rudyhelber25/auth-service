package com.dh.chat.auth.service.web;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author Santiago Mamani
 */
public class AccountAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public AccountAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public AccountAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
