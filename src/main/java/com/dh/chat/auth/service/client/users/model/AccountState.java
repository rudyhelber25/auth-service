package com.dh.chat.auth.service.client.users.model;

/**
 * @author Santiago Mamani
 */
public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}
