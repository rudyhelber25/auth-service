package com.dh.chat.auth.service.client.users.model;

import lombok.Getter;

import java.util.Date;

/**
 * @author Santiago Mamani
 */
public class Account {

    @Getter
    private Long accountId;

    @Getter
    private String email;

    @Getter
    private AccountState accountState;

    @Getter
    private Date createdDate;
}
