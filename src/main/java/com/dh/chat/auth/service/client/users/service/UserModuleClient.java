package com.dh.chat.auth.service.client.users.service;


import com.dh.chat.auth.service.client.users.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Santiago Mamani
 */
@FeignClient(name = "${users.service.name}")
interface UserModuleClient {

    @RequestMapping(
            value = "/system/accounts",
            method = RequestMethod.GET
    )
    User findRootUserByEmail(@RequestParam("email") String email);
}
