package com.dh.chat.auth.service.client.users.exception;

/**
 * @author Santiago Mamani
 */
public class AccountNotFoundException extends RuntimeException {

    //TODO Management to error of feign client
    public AccountNotFoundException(String message) {
        super(message);
    }
}
