package com.dh.chat.auth.service.web;

import lombok.Data;

/**
 * @author Santiago Mamani
 */
@Data
public class UserCredentials {
    private String username, password;
}
