package com.dh.chat.auth.service.client.users.service;

import com.dh.chat.auth.service.client.users.model.Account;
import com.dh.chat.auth.service.client.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class UserModuleService {

    @Autowired
    private UserModuleClient client;

    public User findUserByEmail(String email) {
        return client.findRootUserByEmail(email);
    }
}
