package com.dh.chat.auth.service.client.users.model;

import lombok.Getter;

/**
 * @author Santiago Mamani
 */
public class User {

    @Getter
    private Long userId;

    @Getter
    private Long accountId;

    @Getter
    private String password;

    @Getter
    private String email;

    @Getter
    private AccountState accountState;
}
