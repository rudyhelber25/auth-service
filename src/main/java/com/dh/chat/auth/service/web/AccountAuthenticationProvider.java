package com.dh.chat.auth.service.web;

import com.dh.chat.auth.service.client.users.model.AccountState;
import com.dh.chat.auth.service.client.users.model.User;
import com.dh.chat.auth.service.command.ReadUserCmd;
import com.dh.chat.auth.service.exception.UserAuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * @author Santiago Mamani
 */
@Component
public class AccountAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private ReadUserCmd readUserCmd;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        AccountAuthenticationToken token = (AccountAuthenticationToken) authentication;

        try {
            User rootUser = findRootUser(token.getName(), (String) token.getCredentials());

            validateAccountState(rootUser);

            return buildToken(rootUser);

        } catch (UserAuthenticationException e) {
            throw new BadCredentialsException("Unable to complete authentication process", e);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return AccountAuthenticationToken.class.isAssignableFrom(authentication);
    }

    private JwtAuthenticationToken buildToken(User rootUser) {

        return new JwtAuthenticationToken(rootUser.getAccountId(),
                rootUser.getUserId(),
                rootUser.getEmail(),
                rootUser.getPassword(),
                Collections.emptyList());
    }

    private User findRootUser(String email, String password) {
        readUserCmd.setEmail(email);
        readUserCmd.setPassword(password);
        readUserCmd.execute();

        return readUserCmd.getUser();
    }

    private void validateAccountState(User userRoot) {
        if (AccountState.DEACTIVATED.equals(userRoot.getAccountState())) {
            throw new AccountExpiredException("Account blocked, the activation period has ended");
        }
    }
}
